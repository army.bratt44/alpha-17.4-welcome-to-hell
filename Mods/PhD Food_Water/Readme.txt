PhD Food_Water - 1.0
This mod increases food and water lose by 10 times. 

I created this mod because I found I wasn't using food or water as often as I thought I should. I had installed the great FarmLife Mod by stasis78 and started creating all kinds of great recipes but found I was never using them. I started paying attention to how often I was eating and drinking and decided to make it a bit more challenging. So with this mod you really have to EAT and DRINK and I mean a LOT. It makes farming much more important. 

Try it out and let me know what you think. Oh, and try not to starve. 

I would very much enjoy your feedback.

:: REQUIREMENTS::
• Verions 7.4 


:: FEATURES::
• Increases water lose by 10 times
• Increases food lose by 10 times

